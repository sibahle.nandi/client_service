package com.investec.client.dto.builder;

import com.investec.client.dto.ClientDto;

import java.util.Random;
import java.util.UUID;

public class ClientDtoBuilder {
    public static ClientDto buildClientDto() {
        //using UUID to get unique values on every client dto creation
        ClientDto clientDto = new ClientDto();
        clientDto.setFirstName("Chris" + UUID.randomUUID());
        clientDto.setLastName("Nandi" + UUID.randomUUID());
        clientDto.setMobileNumber("0831234567" + UUID.randomUUID());
        //This is to make sure that we always have a valid 13 digit SA ID number
        Random random = new Random();
        long randomLong = random.nextLong() % 10000000000000L;
        String randomNumber = String.format("%013d", Math.abs(randomLong));
        clientDto.setIdNumber(randomNumber);
        clientDto.setPhysicalAddress("Been around the world " + UUID.randomUUID());
        return clientDto;
    }
}

//b4bca664-921
