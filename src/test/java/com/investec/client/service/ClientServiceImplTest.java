package com.investec.client.service;

import com.investec.client.dto.ClientDto;
import com.investec.client.dto.builder.ClientDtoBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ClientServiceImplTest {
    private ClientServiceImpl clientService;

    @BeforeEach
    void setUpTest() {
        //Todo would use Mockito if there where other services, but for Demo purpose this should do.
        clientService = new ClientServiceImpl();
    }

    @Test
    void searchByName() throws NoSuchFieldException, IllegalAccessException {
        List<ClientDto> clientList = new ArrayList<>();
        ClientDto clientDto = ClientDtoBuilder.buildClientDto();
        clientDto.setFirstName("search by name");

        clientList.add(clientDto);
        clientList.add(ClientDtoBuilder.buildClientDto());
        setPrivateFieldValues(clientList);

        ClientDto client = clientService.searchClient(clientDto);

        assertEquals("search by name", client.getFirstName());
    }

    @Test
    void saveNewClient() throws NoSuchFieldException, IllegalAccessException {
        List<ClientDto> clientList = new ArrayList<>();
        setPrivateFieldValues(clientList);
        ClientDto clientDtoToAdd = ClientDtoBuilder.buildClientDto();

        clientService.saveClient(clientDtoToAdd);

        assertTrue(clientList.contains(clientDtoToAdd));
    }

    @Test
    void updateClientDetailsIfThereIsAnExistingDetails() throws NoSuchFieldException, IllegalAccessException {
        List<ClientDto> clientList = new ArrayList<>();
        ClientDto existingClient = ClientDtoBuilder.buildClientDto();
        clientList.add(existingClient);
        setPrivateFieldValues(clientList);

        ClientDto copyOfClient = new ClientDto();
        copyOfClient.setMobileNumber(existingClient.getIdNumber());
        copyOfClient.setFirstName("Updated first name");
        copyOfClient.setLastName("Updated last name");
        copyOfClient.setMobileNumber("Updated mobile number");
        copyOfClient.setIdNumber(existingClient.getIdNumber());

        clientService.saveClient(copyOfClient);
        ClientDto updateClient = clientList.stream().filter(c -> c.getIdNumber().equals(copyOfClient.getIdNumber())).findFirst().orElse(null);
        assertEquals(copyOfClient, updateClient);
    }

    @Test
    void searchById() throws NoSuchFieldException, IllegalAccessException {
        List<ClientDto> clientList = new ArrayList<>();
        ClientDto clientDto = ClientDtoBuilder.buildClientDto();
        clientDto.setIdNumber("search by Id");
        clientDto.setFirstName(null);

        clientList.add(ClientDtoBuilder.buildClientDto());
        clientList.add(clientDto);
        setPrivateFieldValues(clientList);

        ClientDto client = clientService.searchClient(clientDto);

        assertEquals("search by Id", client.getIdNumber());
    }

    @Test
    void searchByNumber() throws NoSuchFieldException, IllegalAccessException {
        List<ClientDto> clientList = new ArrayList<>();
        ClientDto clientToSearch = ClientDtoBuilder.buildClientDto();
        clientToSearch.setMobileNumber("search by number");
        clientToSearch.setFirstName("other");
        clientToSearch.setFirstName(null);

        clientList.add(ClientDtoBuilder.buildClientDto());
        clientList.add(ClientDtoBuilder.buildClientDto());
        clientList.add(clientToSearch);
        setPrivateFieldValues(clientList);

        ClientDto searchDto = new ClientDto();
        searchDto.setMobileNumber(clientToSearch.getMobileNumber());
        ClientDto client = clientService.searchClient(searchDto);

        assertEquals("search by number", client.getMobileNumber());
    }

    private void setPrivateFieldValues(List<ClientDto> clientList) throws NoSuchFieldException, IllegalAccessException {
        Class<? extends ClientServiceImpl> serviceClass = clientService.getClass();
        Field clientListField = serviceClass.getDeclaredField("clientList");
        clientListField.setAccessible(true);
        clientListField.set(serviceClass, clientList);
    }
}