package com.investec.client.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.investec.client.dto.ClientDto;
import com.investec.client.service.ClientService;
import com.investec.client.validator.ClientValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static com.investec.client.dto.builder.ClientDtoBuilder.buildClientDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = ClientRestController.class)
@AutoConfigureMockMvc
class ClientRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService clientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void saveClient() throws Exception {
        ArgumentCaptor<ClientDto> clientDtoArgumentCaptor = ArgumentCaptor.forClass(ClientDto.class);
        ClientDto clientRequest = buildClientDto();
        mockMvc.perform(post("/api/client/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRequest)))
                .andExpect(status().isOk());

        verify(clientService).saveClient(clientDtoArgumentCaptor.capture());

        assertEquals(clientRequest, clientDtoArgumentCaptor.getValue());
    }

    @Test
    void returnSavedMessageToClient() throws Exception {
        ClientDto clientRequest = buildClientDto();
        ResultActions resultActions = mockMvc.perform(post("/api/client/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRequest)))
                .andExpect(status().isOk());

        assertEquals("Client updated/saved successfully", resultActions.andReturn().getResponse().getContentAsString());

    }

    @Test
    void validateFirstNameMandatory() throws Exception {
        ClientDto clientRequest = buildClientDto();
        clientRequest.setFirstName(null);
        ResultActions resultActions = mockMvc.perform(post("/api/client/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andDo(print());
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertTrue(response.getContentAsString().contains("First name required"));
    }

    @Test
    void validateLastNameMandatory() throws Exception {
        ClientDto clientRequest = buildClientDto();
        clientRequest.setLastName(null);
        ResultActions resultActions = mockMvc.perform(post("/api/client/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andDo(print());
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertTrue(response.getContentAsString().contains("Last name required"));
    }

    @Test
    void validateIdNumberMandatory() throws Exception {
        ClientDto clientRequest = buildClientDto();
        clientRequest.setIdNumber(null);
        clientRequest.setFirstName(null);
        ResultActions resultActions = mockMvc.perform(post("/api/client/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(clientRequest)))
                .andExpect(status().isBadRequest())
                .andDo(print());
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        assertTrue(response.getContentAsString().contains("Id number required"));
    }

    @Test
    void returnDtoFromServiceWhenSearchingForClient() throws Exception {
        ArgumentCaptor<ClientDto> clientDtoArgumentCaptor = ArgumentCaptor.forClass(ClientDto.class);
        ClientDto returnedClientDto = buildClientDto();
        ClientDto clientRequest = buildClientDto();
        when(clientService.searchClient(any(ClientDto.class))).thenReturn(returnedClientDto);
        mockMvc.perform(post("/api/client/search")
                .content(objectMapper.writeValueAsString(clientRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(returnedClientDto)))
                .andDo(print());

        verify(clientService).searchClient(clientDtoArgumentCaptor.capture());

        assertEquals(clientRequest, clientDtoArgumentCaptor.getValue());
    }

    @Test
    void rethrowExceptionWhenSearchingForTheClient() throws Exception {
        when(clientService.searchClient(any(ClientDto.class))).thenThrow(new RuntimeException("Runtime exception"));

        ResultActions resultActions = mockMvc.perform(post("/api/client/search")
                .content(objectMapper.writeValueAsString(buildClientDto()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError());

        MockHttpServletResponse response = resultActions.andReturn().getResponse();

        String errorMessage = response.getErrorMessage();

        verify(clientService).searchClient(any(ClientDto.class));
        assertEquals(errorMessage, "Error while searching for client, contact Admin");
    }

    @Test
    void rethrowExceptionWhenSavingTheClient() throws Exception {
        doThrow(new RuntimeException("Runtime exception during a save"))
                .when(clientService).saveClient(any(ClientDto.class));

        ResultActions resultActions = mockMvc.perform(post("/api/client/save")
                .content(objectMapper.writeValueAsString(buildClientDto()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andDo(print());

        MockHttpServletResponse response = resultActions.andReturn().getResponse();

        String errorMessage = response.getErrorMessage();

        verify(clientService).saveClient(any(ClientDto.class));
        assertEquals(errorMessage, "Error could not save client, contact Admin");
    }


    @Test
    void rethrowExceptionWhenSearchingForPokemon() throws Exception {
        when(clientService.searchClient(any())).thenThrow(new RuntimeException("Runtime exception"));

        mockMvc.perform(post("/api/client/search").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(buildClientDto())))
                .andExpect(status().isInternalServerError())
                .andDo(print());

        verify(clientService).searchClient(any(ClientDto.class));
    }
}