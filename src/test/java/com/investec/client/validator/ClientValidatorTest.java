package com.investec.client.validator;

import com.investec.client.dto.ClientDto;
import com.investec.client.dto.builder.ClientDtoBuilder;
import com.investec.client.service.ClientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientValidatorTest {

    @InjectMocks
    private ClientValidator clientValidator;
    @Mock
    private ClientService clientService;


    @Test
    void searchClientByMobileNumber() {
        ClientDto clientDtoToValidate = ClientDtoBuilder.buildClientDto();
        ArgumentCaptor<ClientDto> clientDtoArgumentCaptor = ArgumentCaptor.forClass(ClientDto.class);
        when(clientService.searchClient(any(ClientDto.class))).thenReturn(ClientDtoBuilder.buildClientDto());

        clientValidator.isValid(clientDtoToValidate, mock(ConstraintValidatorContext.class));

        verify(clientService).searchClient(clientDtoArgumentCaptor.capture());
        assertAll("Client attributes",
                () -> assertEquals(clientDtoToValidate.getMobileNumber(), clientDtoArgumentCaptor.getValue().getMobileNumber()),
                () -> assertNull(clientDtoArgumentCaptor.getValue().getIdNumber()),
                () -> assertNull(clientDtoArgumentCaptor.getValue().getFirstName()),
                () -> assertNull(clientDtoArgumentCaptor.getValue().getLastName()),
                () -> assertNull(clientDtoArgumentCaptor.getValue().getPhysicalAddress()));
    }

    @Test
    void shouldReturnFalseWhenInvalidSAID() {
        ClientDto clientDtoToValidate = ClientDtoBuilder.buildClientDto();
        clientDtoToValidate.setIdNumber("invalid id number");

        boolean valid = clientValidator.isValid(clientDtoToValidate, mock(ConstraintValidatorContext.class));

        assertFalse(valid);
        verify(clientService, never()).searchClient(any(ClientDto.class));
    }

    @Test
    void shouldReturnFalseIDisNull() {
        ClientDto clientDtoToValidate = ClientDtoBuilder.buildClientDto();
        clientDtoToValidate.setIdNumber(null);

        boolean valid = clientValidator.isValid(clientDtoToValidate, mock(ConstraintValidatorContext.class));

        assertFalse(valid);
        verify(clientService, never()).searchClient(any(ClientDto.class));
    }

    @Test
    void shouldReturnFalseDifferentIdNumbersButSameContactDetails() {
        ClientDto clientDtoToValidate = ClientDtoBuilder.buildClientDto();
        ClientDto existingClient = ClientDtoBuilder.buildClientDto();
        clientDtoToValidate.setMobileNumber(existingClient.getMobileNumber());

        when(clientService.searchClient(any(ClientDto.class))).thenReturn(existingClient);

        boolean valid = clientValidator.isValid(clientDtoToValidate, mock(ConstraintValidatorContext.class));

        assertFalse(valid);
        verify(clientService).searchClient(any(ClientDto.class));
    }

    @Test
    void shouldReturnTrueIfThereIsNoClientWithExistingPhoneNumber() {
        ClientDto clientDtoToValidate = ClientDtoBuilder.buildClientDto();
        ClientDto existingClient = ClientDtoBuilder.buildClientDto();
        clientDtoToValidate.setMobileNumber(existingClient.getMobileNumber());

        when(clientService.searchClient(any(ClientDto.class))).thenReturn(null);

        boolean valid = clientValidator.isValid(clientDtoToValidate, mock(ConstraintValidatorContext.class));

        assertTrue(valid);
        verify(clientService).searchClient(any(ClientDto.class));
    }
}