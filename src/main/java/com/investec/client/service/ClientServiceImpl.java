package com.investec.client.service;


import com.investec.client.dto.ClientDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {
    //Since there is no need for a DB we can use this Collection Data Structure for data purpose
    private static List<ClientDto> clientList = new ArrayList<>();


    @Override
    public ClientDto searchClient(ClientDto clientDto) {
        //TODO: Null checks are just for testing purpose since the search fields are all mandatory when saving and updating
        return clientList.stream().
                filter(c -> (c.getFirstName() != null && c.getFirstName().equals(clientDto.getFirstName()))
                        || (c.getIdNumber() != null && c.getIdNumber().equals(clientDto.getIdNumber()))
                        || c.getMobileNumber() != null && c.getMobileNumber().equals(clientDto.getMobileNumber()))
                .findAny()
                .orElse(null);
    }

    @Override
    public void saveClient(ClientDto clientDto) {
        ClientDto existingClient = clientList.stream().filter(cl -> cl.getIdNumber().equals(clientDto.getIdNumber()))
                .findFirst().orElse(null);
        if (existingClient == null) {
            // add new client to list
            clientList.add(clientDto);
        } else {
            existingClient.setFirstName(clientDto.getFirstName());
            existingClient.setLastName(clientDto.getLastName());
            existingClient.setPhysicalAddress(clientDto.getPhysicalAddress());
            existingClient.setMobileNumber(clientDto.getMobileNumber());
        }
    }
}
