package com.investec.client.service;


import com.investec.client.dto.ClientDto;

public interface ClientService {

    ClientDto searchClient(ClientDto clientDto);

    void saveClient(ClientDto clientDto);
}
