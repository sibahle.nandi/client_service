package com.investec.client.validator;

import com.investec.client.dto.ClientDto;
import com.investec.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ClientValidator implements ConstraintValidator<ClientValidation, ClientDto> {
    @Autowired
    private ClientService clientService;

    @Override
    public boolean isValid(ClientDto clientValidating, ConstraintValidatorContext context) {


        if (clientValidating.getIdNumber() == null || !clientValidating.getIdNumber().matches("^\\d{13}$")) {
            return false;
        }

        ClientDto clientDto = new ClientDto();
        clientDto.setMobileNumber(clientValidating.getMobileNumber());
        ClientDto existingClient = clientService.searchClient(clientDto);
        return isSameClient(existingClient, clientValidating);
    }

    private boolean isSameClient(ClientDto existingClient, ClientDto clientValidating) {
        return existingClient == null || existingClient.getIdNumber().equals(clientValidating.getIdNumber());
    }
}
