package com.investec.client.controller;

import com.investec.client.dto.ClientDto;
import com.investec.client.service.ClientService;
import com.investec.client.validator.ClientValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@Validated
public class ClientRestController {

    public static final String SEARCHING_FOR_CLIENT_CONTACT_ADMIN_MESSAGE = "Error while searching for client, contact Admin";
    public static final String SAVE_CLIENT_CONTACT_ADMIN_MESSAGE = "Error could not save client, contact Admin";
    public static final String API_CLIENT_SEARCH = "/api/client/search";
    public static final String API_CLIENT_SAVE = "/api/client/save";
    public static final String UPDATED_SAVED_SUCCESSFULLY = "Client updated/saved successfully";
    @Autowired
    private ClientService pokemonService;

    @PostMapping(API_CLIENT_SAVE)
    public ResponseEntity<String>
    saveClient(@Valid @RequestBody @ClientValidation ClientDto clientDto) {
        try {
            pokemonService.saveClient(clientDto);
            return ResponseEntity.ok(UPDATED_SAVED_SUCCESSFULLY);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, SAVE_CLIENT_CONTACT_ADMIN_MESSAGE);
        }
    }

    @PostMapping(API_CLIENT_SEARCH)
    public @ResponseBody
    ClientDto findPokemonByName(@RequestBody ClientDto clientDto) {
        try {
            return pokemonService.searchClient(clientDto);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, SEARCHING_FOR_CLIENT_CONTACT_ADMIN_MESSAGE);
        }
    }
}
